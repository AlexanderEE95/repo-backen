package com.Wigelkoncern.biluthyrning.execptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RescourceNotFoundExecption extends RuntimeException{

    private static final long serialVersionUID = 1L;
    private String name;
    private String field;
    private Object value;

    public RescourceNotFoundExecption(String name, String field , Object value){
        super(String.format("%s not foind with %s : '%s'", name, field, value));
        this.name = name;
        this.field = field;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }
}
