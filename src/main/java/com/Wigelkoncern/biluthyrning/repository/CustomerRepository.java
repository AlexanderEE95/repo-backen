package com.Wigelkoncern.biluthyrning.repository;

import com.Wigelkoncern.biluthyrning.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findByUserName(String userName);
}
