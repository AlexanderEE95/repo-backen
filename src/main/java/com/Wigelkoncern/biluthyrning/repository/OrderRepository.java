package com.Wigelkoncern.biluthyrning.repository;

import com.Wigelkoncern.biluthyrning.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order>findByCustomer_id(int customer_id);
    List<Order>findByCar_id(int car_id);
}
