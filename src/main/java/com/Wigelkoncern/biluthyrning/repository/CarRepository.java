package com.Wigelkoncern.biluthyrning.repository;

import com.Wigelkoncern.biluthyrning.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}
