package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Order;
import com.Wigelkoncern.biluthyrning.execptions.RescourceNotFoundExecption;
import com.Wigelkoncern.biluthyrning.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService implements OrderServiceInterface{

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CarService carService;

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> ordersByCustomerId(int id) {
        return orderRepository.findByCustomer_id(id);
    }

    @Override
    public Order cancelorder(int id) {
        Order o = orderRepository.findById(id).orElseThrow(() -> new RescourceNotFoundExecption("order", "id", id));
        o.setCanceled(true);
        orderRepository.save(o);
        return o;
    }

    @Override
    public Order updateOrder(Order order, int id) {
        Order o = orderRepository.findById(order.getId()).orElseThrow(() -> new RescourceNotFoundExecption("order", "id", order.getId()));
        o.setStartDate(order.getStartDate());
        o.setReturnDate(order.getReturnDate());
        o.setCustomer(customerService.findById(order.getCustomer().getId()));
        o.setCar(carService.findById(order.getCar().getId()));
        o.setCanceled(order.isCanceled());
        orderRepository.save(o);
        return o;
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> ordersByCarId(int id) {
        return orderRepository.findByCar_id(id);
    }


}
