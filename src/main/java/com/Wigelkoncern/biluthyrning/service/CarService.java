package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Car;
import com.Wigelkoncern.biluthyrning.execptions.RescourceNotFoundExecption;
import com.Wigelkoncern.biluthyrning.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService implements CarServiceInterface{
    @Autowired
    private CarRepository carRepository;


    @Override
    public Car findById(int id) {
        return carRepository.findById(id).orElseThrow(() -> new RescourceNotFoundExecption("car", "id", id));
    }

    @Override
    public Car saveCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public void deleteCar(int id) {
        carRepository.findById(id).orElseThrow(() -> new RescourceNotFoundExecption("car", "id", id));
        carRepository.deleteById(id);
    }

    @Override
    public Car updateCar(Car car, int id) {
        Car c = carRepository.findById(id).orElseThrow(()-> new RescourceNotFoundExecption("car", "id", id));
        if(car.getName() == null){

        }else{
            c.setName(car.getName());
        }
        if(car.getModell() == null){
        }else {
            c.setModell(car.getModell());
        }
        if(car.getPricePerDay() == 0){

        }else{
            c.setPricePerDay(car.getPricePerDay());
        }
        carRepository.save(c);
        return c;
    }

    @Override
    public List<Car> getALLCars() {
        return carRepository.findAll();
    }
}
