package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Customer;

import java.util.List;

public interface CustomerServiceInterface {
    Customer findById(int id);
    Customer saveCustomer(Customer customer);
    List<Customer> getAllCustomers();
    Customer findByUsername(String userName);
}
