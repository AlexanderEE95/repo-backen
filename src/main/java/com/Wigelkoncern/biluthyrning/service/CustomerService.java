package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Customer;
import com.Wigelkoncern.biluthyrning.execptions.RescourceNotFoundExecption;
import com.Wigelkoncern.biluthyrning.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements CustomerServiceInterface {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer findById(int id) {
        return customerRepository.findById(id).orElseThrow(() -> new RescourceNotFoundExecption("name ", "id", id)) ;
    }

    @Override
    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer findByUsername(String userName) {
        return customerRepository.findByUserName(userName);
    }
}
