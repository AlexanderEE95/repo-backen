package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Order;

import java.util.List;

public interface OrderServiceInterface {
    Order saveOrder(Order order);
    List<Order> ordersByCustomerId(int id);
    Order cancelorder(int id);
    Order updateOrder(Order order, int id);
    List<Order> getAllOrders();
    List<Order> ordersByCarId(int id);
}
