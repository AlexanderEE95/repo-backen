package com.Wigelkoncern.biluthyrning.service;

import com.Wigelkoncern.biluthyrning.entity.Car;

import java.util.List;

public interface CarServiceInterface {
    Car findById(int id);
    Car saveCar(Car car);
    void deleteCar(int id);
    Car updateCar(Car car, int id);
    List<Car> getALLCars();
}
