package com.Wigelkoncern.biluthyrning.VO;

public class ResponseTemplateVO {
    private Currency currency;

    public ResponseTemplateVO() {
    }

    public ResponseTemplateVO(Currency currency) {

        this.currency = currency;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
