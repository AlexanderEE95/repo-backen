package com.Wigelkoncern.biluthyrning.VO;

public class Currency {

    private int currencyId;
    private double currency;

    public Currency() {
    }

    public Currency(int currencyId, double currency) {
        this.currencyId = currencyId;
        this.currency = currency;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public double getCurrency() {
        return currency;
    }

    public void setCurrency(double currency) {
        this.currency = currency;
    }
}
