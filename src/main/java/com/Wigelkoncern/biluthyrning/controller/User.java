package com.Wigelkoncern.biluthyrning.controller;

import com.Wigelkoncern.biluthyrning.entity.Car;
import com.Wigelkoncern.biluthyrning.entity.Customer;
import com.Wigelkoncern.biluthyrning.entity.Order;
import com.Wigelkoncern.biluthyrning.service.CarService;
import com.Wigelkoncern.biluthyrning.service.CustomerService;
import com.Wigelkoncern.biluthyrning.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
public class User {
    @Autowired
    private CarService carService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private RestTemplate restTemplate;



    @GetMapping("/login")
    public int getUserName(@RequestHeader String userName){
        int userId = 0;
        List<Customer> customers = customerService.getAllCustomers();
        List<String> usernames = customers.stream().map(Customer::getUserName).collect(Collectors.toList());

        if(usernames.contains(userName)){
            userId = customerService.findByUsername(userName).getId();
        }
        return userId;
    }

    @GetMapping("/cars")
    public List<Car>getAllCars(@RequestHeader String startDay, @RequestHeader String returnDay) {

        LocalDate start = LocalDate.parse(startDay);
        LocalDate endDate = LocalDate.parse(returnDay);

        List<Car> cars = carService.getALLCars();
        List<Order> orders = orderService.getAllOrders();
        for (Order order : orders) {
            if ( (start.isAfter(order.getStartDate()) || start.isEqual(order.getStartDate())) && (start.isBefore(order.getReturnDate()) || start.isEqual(order.getReturnDate())) ) {
                if (!order.isCanceled()) {
                    cars.remove(order.getCar());
                }
            }
            else if ( (endDate.isAfter(order.getStartDate()) || endDate.isEqual(order.getStartDate())) && (endDate.isBefore(order.getReturnDate()) || endDate.isEqual(order.getReturnDate())) ) {
                if (!order.isCanceled()) {
                    cars.remove(order.getCar());
                }
            }
        }

        return cars;
    }

    @PostMapping("/ordercar")
    public ResponseEntity<Order> saveOrder(@RequestBody Order order){
        order.setCar(carService.findById(order.getCar().getId()));
        order.setCustomer(customerService.findById(order.getCustomer().getId()));
        return new ResponseEntity<>(orderService.saveOrder(order), HttpStatus.CREATED);
    }

    @PutMapping("/updateorder")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order){
        return new ResponseEntity<>(orderService.updateOrder(order, order.getId()), HttpStatus.OK);
    }

    @GetMapping("/myorders")
    public List<Order> getCustomersOrders(@RequestHeader int id){
        return orderService.ordersByCustomerId(id);
    }

    @GetMapping("/exchange")
    public double getEuroCost(@RequestBody int id){
        double vo = 0;
        if(restTemplate.getForObject("http://CURRENCY-EXCHANGE/exchange/" + carService.findById(id).getPricePerDay(), double.class) == null){
            System.out.println("CURRENCY-EXCHANGE is not working");
        }else{
           vo = restTemplate.getForObject("http://CURRENCY-EXCHANGE/exchange/" + carService.findById(id).getPricePerDay(), double.class);
        }
        return vo;
    }


}
