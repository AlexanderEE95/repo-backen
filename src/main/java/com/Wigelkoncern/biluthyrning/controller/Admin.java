package com.Wigelkoncern.biluthyrning.controller;

import com.Wigelkoncern.biluthyrning.entity.Car;
import com.Wigelkoncern.biluthyrning.entity.Customer;
import com.Wigelkoncern.biluthyrning.entity.Order;
import com.Wigelkoncern.biluthyrning.service.CarService;
import com.Wigelkoncern.biluthyrning.service.CustomerService;
import com.Wigelkoncern.biluthyrning.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class Admin {


    @Autowired
    private CustomerService customerService;

    @Autowired
    private CarService carService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/customers")
    public List<Customer> getAllCustomers(){
        return customerService.getAllCustomers();
    }

    @GetMapping("/admin/cars")
    public List<Car> getCars(){
        return carService.getALLCars();
    }

    @GetMapping("/OrdersOfCustomer")
    public int getNumberOfOrders(@RequestHeader int id){
        return orderService.ordersByCustomerId(id).size();
    }

    @PostMapping("/addCustomer")
    public ResponseEntity<Customer> saveCustomer(@RequestBody Customer customer){
        return new ResponseEntity<Customer>(customerService.saveCustomer(customer), HttpStatus.CREATED);
    }

    @PostMapping("/addcar")
    public ResponseEntity<Car> saveCar(@RequestBody Car car){
        return new ResponseEntity<Car>(carService.saveCar(car), HttpStatus.CREATED);
    }

    @PutMapping("/updateCar")
    public ResponseEntity<Car> updateCar(@RequestBody Car car){
        return new ResponseEntity<Car>(carService.updateCar(car, car.getId()), HttpStatus.OK);
    }

    @PutMapping("/cancelorder")
    public ResponseEntity<Order> cancelorder(@RequestHeader int orderId){
        return new ResponseEntity<Order>(orderService.cancelorder(orderId), HttpStatus.OK);
    }

    @DeleteMapping("/deletcar")
    public ResponseEntity<String> deletCar(@RequestBody Car car){
        List<Order> orders = orderService.ordersByCarId(car.getId());
        for(int i = 0; i < orders.size(); i++){
            orders.get(i).setCar(null);
        }
        carService.deleteCar(car.getId());
        return new ResponseEntity<String>("Car Deleted with id " + car.getId(), HttpStatus.OK);
    }

}
