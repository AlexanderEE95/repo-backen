package com.Wigelkoncern.biluthyrning.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.List;

@EnableWebSecurity @Configuration
public class BasicSecurityConfig extends WebSecurityConfigurerAdapter{

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication()
                .withUser("kalle")
                .password("kalle123")
                .roles("USER")
                .and()
                .withUser("bobo")
                .password("bobo123")
                .roles("USER")
                .and()
                .withUser("bärtan")
                .password("bärtan123")
                .roles("USER")
                .and()
                .withUser("Lilla")
                .password("Lilla123")
                .roles("USER")
                .and()
                .withUser("anka")
                .password("anka123")
                .roles("ADMIN");
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http

               .csrf().disable()
               .authorizeRequests()
               .antMatchers("/api/v1/addCustomer").hasRole("ADMIN")
               .antMatchers("/api/v1/customers").hasRole("ADMIN")
               .antMatchers("/api/v1/addcar").hasRole("ADMIN")
               .antMatchers("/api/v1/updateCar").hasRole("ADMIN")
               .antMatchers("/api/v1/cancelorder").hasRole("ADMIN")
               .antMatchers("/api/v1/deletcar").hasRole("ADMIN")
               .antMatchers("/api/v1/OrdersOfCustomer").hasRole("ADMIN")
               .antMatchers("/api/v1/admin/cars").hasRole("ADMIN")
               .antMatchers("/api/v1/ordercar").hasRole("USER")
               .antMatchers("/api/v1/updateorder").hasRole("USER")
               .antMatchers("/api/v1/myorders").hasRole("USER")
               .antMatchers("/api/v1/cars").hasRole("USER")
               .antMatchers("/api/v1/exchange").hasRole("USER")
               .antMatchers("/api/v1/login").hasAnyRole("USER","ADMIN")
               .and()
               .httpBasic();
       http.headers().frameOptions().disable();
       http.cors().configurationSource(request -> corsFilter());
    }
    @Bean
    public CorsConfiguration corsFilter(){
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("http://localhost:5500");
        config.addAllowedOrigin("http://127.0.0.1:5500/");
        config.addAllowedOrigin("http://localhost:5501");
        config.addAllowedOrigin("http://127.0.0.1:5501/");
        config.setAllowedMethods(List.of("GET", "POST", "OPTIONS", "DELETE", "PUT"));
        config.setAllowedHeaders(List.of("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization", "Access-Control-Allow-Origin", "startDay", "returnDay", "userName", "id"));
        config.setExposedHeaders(List.of("Authorization"));
        return config;
    }
}
