package com.Wigelkoncern.biluthyrning;

import com.Wigelkoncern.biluthyrning.entity.Car;
import com.Wigelkoncern.biluthyrning.entity.Customer;
import com.Wigelkoncern.biluthyrning.entity.Order;
import com.Wigelkoncern.biluthyrning.service.CarService;
import com.Wigelkoncern.biluthyrning.service.CustomerService;
import com.Wigelkoncern.biluthyrning.service.OrderService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;


@SpringBootApplication
public class BilutHyrningApplication {

    public static void main(String[] args) {

        SpringApplication.run(BilutHyrningApplication.class, args);
    }
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
    @Bean
    public CommandLineRunner init(CarService carService, CustomerService customerService, OrderService orderService){
        return args -> {

            Car car1 = creatMockCar("volvo","X40", 1100);
            Car car2 = creatMockCar("volvo","V60", 1300);
            Car car3 = creatMockCar("audi", "R8",2500);
            Car car4 = creatMockCar("bergare","TOMAT", 500);
            Car car5 = creatMockCar("tesla","speed", 3000);
            Car car6 = creatMockCar("ford", "cros",1500);
            Car car7 = creatMockCar("kia","kaktus", 2300);
            Car car8 = creatMockCar("audi","A3", 1580);

            carService.saveCar(car1);
            carService.saveCar(car2);
            carService.saveCar(car3);
            carService.saveCar(car4);
            carService.saveCar(car5);
            carService.saveCar(car6);
            carService.saveCar(car7);
            carService.saveCar(car8);



            Customer customer1 = creatMockCustomer("bobo", "bo", "Bovägen 90");
            Customer customer2 = creatMockCustomer("kalle", "anka", "KalleVägen 88");
            Customer customer3 = creatMockCustomer("bärtan", "bärtil", "SvängVägen 22");
            Customer customer4 = creatMockCustomer("Lilla", "lillia", "SovVägen 60");

            customerService.saveCustomer(customer1);
            customerService.saveCustomer(customer2);
            customerService.saveCustomer(customer3);
            customerService.saveCustomer(customer4);

            Order order1 = creatMockOrder("2022-10-28","2020-10-31");
            Order order2 = creatMockOrder("2022-10-28","2020-10-31");
            Order order3 = creatMockOrder("2022-10-28","2020-10-31");
            Order order4 = creatMockOrder("2022-10-28","2020-10-31");
            Order order5 = creatMockOrder("2022-10-28","2020-10-31");

            orderService.saveOrder(order1);
            orderService.saveOrder(order2);
            orderService.saveOrder(order3);
            orderService.saveOrder(order4);
            orderService.saveOrder(order5);

            order1.setCar(car1);
            order1.setCustomer(customer1);
            orderService.saveOrder(order1);

            order2.setCar(car2);
            order2.setCustomer(customer2);
            orderService.saveOrder(order2);

            order3.setCar(car3);
            order3.setCustomer(customer3);
            orderService.saveOrder(order3);

            order4.setCar(car4);
            order4.setCustomer(customer4);
            orderService.saveOrder(order4);

            order5.setCar(car4);
            order5.setCustomer(customer2);
            orderService.saveOrder(order5);
        };
    }
    private Car creatMockCar(String name, String modell, int pricePerDay){
        return new Car(name,modell,pricePerDay);
    }
    private Customer creatMockCustomer(String userName, String name, String address){
        return new Customer(userName,name,address);
    }
    private Order creatMockOrder(String startDate, String returnDate){
        Order mockOrder = new Order(LocalDate.parse(startDate),LocalDate.parse(returnDate));

        return mockOrder;
    }


/*
    @Bean
    public KeycloakSpringBootConfigResolver keycloakSpringBootConfigResolver (){
        return new KeycloakSpringBootConfigResolver();
    }
*/

}
